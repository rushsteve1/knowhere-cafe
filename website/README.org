#+TITLE: Knowhere Cafe Website

This is the frontend website of Knowhere Cafe and is the bulk of the project.
It is written in [[https://clojurescript.org][ClojureScript]] using [[https://github.com/day8/re-frame][Re-frame]].

The documentation provided by Re-frame can be found in [[PROVIDED.org][=PROVIDED.org=]] and it is
a very thorough introduction to this portion of the project.

This blog series by PurelyFunctional.tv is also a great resource:
https://purelyfunctional.tv/guide/re-frame-building-blocks/
