(ns knowhere-cafe.css
  (:require [garden.def :refer [defstyles]]
            [garden.color :as color]))

(def background-color "#170f11")
(def primary-color "#402039")
(def accent-color "#5c164e")
(def highlight-color "#c80073")
(def text-color "#e2fcef")

(defstyles screen
  ["@font-face" {:font-family "'vt323'"
                 :weight :normal
                 :style :normal
                 :src "url('/static/vt323-regular-webfont.woff2') format('woff2'),
     url('/static/vt323-regular-webfont.woff') format('woff')"}]
  ["::selection" {:background-color highlight-color}]
  ["*::-moz-focus-inner, :focus" {:border :none
                                  :outline :none}]
  [:body {:color text-color
          :font-family "'vt323', 'Consolas', monospace"
          :font-size "14pt"
          :background-color background-color
          :margin 0
          :padding 0}]

  [:a {:color :inherit
       :outline :none
       :text-decoration-style :dashed}
   [:&:hover {:text-decoration-style "solid"}]]

  [:h1 {:margin 0}]

  [:input :textarea :button
   {:outline :none
    :border :none
    :background-color :inherit
    :font-family :inherit
    :font-size :inherit
    :color :inherit
    :padding "0.3em"}]

  [:form
   [:input :textarea :button
    {:display :block}]]

  [:header {:background-color primary-color
            :height "7rem"
            :margin-bottom "3rem"
            :transition "all 1s"}
   [:&.full {:height "100vh"
             :margin-bottom 0
             :font-size "150%"}
    [:img {:height "15rem"
           :width "15rem"}]]
   [:img {:height "6rem"
          :width "6rem"
          :transition "height 1s, width 1s"}]]

  [:main {:max-width "50rem"
          :margin :auto}]

  [:.row {:display :flex
          :flex-flow "row wrap"
          :justify-content :space-around}]

  [:.col {:display :flex
          :flex-flow "column wrap"
          :justify-content :center
          :align-items :center
          :align-content :center}
   [:* {:margin "0 1rem"}]]

  [:.card {:padding "2rem"
           :background-color primary-color
           :border (str "2px solid" accent-color)}
   [:input :textarea :button
    {:background-color (color/darken primary-color 4)}]]

  [:#player {:position :fixed
             :bottom 0
             :height "3rem"
             :width "100vw"
             :background-color primary-color
             :transition "height 1s"
             :flex-wrap :wrap}
   [:&.tall {:height "5rem"
             :font-size "150%"}]
   [:&.full {:height "84vh"
             :font-size "200%"
             :margin :auto
             :background-color background-color}]
   [:button {:font-size "2rem"
             :padding 0
             :transition "font-size 1s"}]])
