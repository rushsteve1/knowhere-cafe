(ns knowhere-cafe.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re-frame/reg-sub
 ::at-home
 (fn [db _]
   (= :home-panel (:active-panel db))))

(re-frame/reg-sub
 ::logged-in-user
 (fn [db _]
   (:logged-in-user db)))
