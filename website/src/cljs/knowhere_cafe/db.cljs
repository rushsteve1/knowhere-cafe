(ns knowhere-cafe.db)

(def default-db
  {:name "Knowhere.Cafe"
   :active-panel :home-panel
   :logged-in-user nil})
