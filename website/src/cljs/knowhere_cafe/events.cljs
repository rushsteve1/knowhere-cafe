(ns knowhere-cafe.events
  (:require
   [re-frame.core :as re-frame]
   [knowhere-cafe.db :as db]
   [knowhere-cafe.effects :as effects]
   [day8.re-frame.tracing :refer-macros [fn-traced]]))


(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn-traced [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
 ::set-re-pressed-example
 (fn-traced [db [_ value]]
            (assoc db :re-pressed-example value)))

(re-frame/reg-event-fx
 ::login-and-redirect
 (fn-traced [cofx [_ fields]]
            {::effects/login fields}))

(re-frame/reg-event-db
 ::set-user
 (fn-traced [db [_ user]]
            (assoc db :logged-in-user user)))
