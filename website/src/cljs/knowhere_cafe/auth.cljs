(ns knowhere-cafe.auth
  (:require [re-frame.core :refer [dispatch]]
            ["gotrue-js" :default GoTrue]))


;; Authentication is with Netlify Auth and is built on the
;; GoTrue-JS library which is linked below.
;; https://github.com/netlify/gotrue-js

(def auth (GoTrue. {:APIUrl "https://knowhere.cafe/.netlify/identity"
                    :audience ""
                    :setCookie false}))

(defn login
  "Login to the Netlify authentication system with a username and password"
  ([{:keys [:email :password]}]
   (login email password))
  ([email password]
   (-> auth
       (.login (clj->js email) (clj->js password))
       (.then #(dispatch [:knowhere-cafe.events/set-user
                          (:response (js->clj %))]))
       (.catch #(js/console.error %)))
   'ok))

(defn current-user
  "Returns the currently logged-in user as a Clojure map"
  []
  (js->clj (.currentUser auth)))

(defn logout
  "Log out the current user"
  []
  (-> auth
      (.logout)
      (.then #(println "logout"))
      (.catch #(js/console.error %))))
