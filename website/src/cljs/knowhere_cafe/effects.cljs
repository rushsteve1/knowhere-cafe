(ns knowhere-cafe.effects
  (:require [re-frame.core :as re-frame]
            [knowhere-cafe.auth :as auth]
            [day8.re-frame.tracing :refer-macros [fn-traced]]))

(re-frame/reg-fx
 ::login
 (fn-traced [fields]
            (auth/login fields)
            (secretary.core/dispatch! "/dash")))
