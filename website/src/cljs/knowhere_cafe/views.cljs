(ns knowhere-cafe.views
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [knowhere-cafe.events :as events]
   [knowhere-cafe.routes :as routes]
   [knowhere-cafe.subs :as subs]))

(defn header-bar []
  (let [at-home (re-frame/subscribe [::subs/at-home])
        user (re-frame/subscribe [::subs/logged-in-user])]
    [:header.col (when @at-home {:class "full"})
     [:img {:src "/icons/knowhere-cafe-icon.svg"
            :alt "Still Warm"
            :title "Still Warm"}]
     [:div.col
      [:a {:href (routes/home)}
       [:h1 "Knowhere Cafe"]]
      (if @user
        [:div
         [:a {:href (routes/dash)} "Dash"]
         [:a {:href ""} "Other"]]
        [:a {:href (routes/login)} "ENTER"])
      [:div
       [:a {:href (routes/about)} "About"]
       [:a {:href (routes/radio)} "Radio"]]]]))

;; auth

(defn login-panel []
  (let [fields (reagent/atom {:email "" :password ""})]
    (fn []
      [:form.card {:on-submit
                       #(do (.preventDefault %)
                            (re-frame/dispatch
                             [::events/login-and-redirect @fields]))}
       [:input {:type :email
                :autoFocus true
                :autoComplete :off
                :name :email
                :placeholder "eMail"
                :value (:email @fields)
                :on-change #(swap! fields assoc :email (-> % .-target .-value))}]
       [:input {:type :password
                :name :password
                :placeholder "Passcode"
                :value (:password @fields)
                :on-change #(swap! fields assoc :password (-> % .-target .-value))}]
       [:input {:type :submit}]])))


;; about

(defn about-panel []
  [:div
   [:p "Oh... Hi. Thing's are still very under construction."]])


;; radio

(defn music-player []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])
        audio-elm (reagent/atom nil)]
    (fn []
      [:div#player.col {:class (case @active-panel
                                 :home-panel "tall"
                                 :radio-panel "full"
                                 nil)}
       [:audio {:ref #(reset! audio-elm %) :src "https://radio.knowhere.cafe/stream.ogg"}]
       (when-let [elm @audio-elm]
         [:button {:on-click #(if (.-paused elm) (.play elm) (.pause elm))
                   :style (when (= @active-panel :radio-panel) {:font-size "6rem"})}
          (if (.-paused elm) "|>" "||")])
       [:div.row
        [:span "Song Title"]
        [:span "Album Title"]
        [:span "Set Title"]]])))


;; dashboard

(defn dash-panel []
  [:p "welcome, glad you could make it. You're logged in, but there's nothing to see yet."])

;; main


(defn show-panel [panel-name]
  [:main
   (case panel-name
     :about-panel [about-panel]
     :dash-panel  [dash-panel]
     :login-panel [login-panel]
     :home-panel  [:div]
     :radio-panel [:div]
     [:div "Huh? How'd you get this lost?"])])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [:div
     [header-bar]
     [show-panel @active-panel]
     [music-player]]))
