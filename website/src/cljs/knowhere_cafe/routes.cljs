(ns knowhere-cafe.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
   [secretary.core :as secretary]
   [goog.events :as gevents]
   [re-frame.core :as re-frame]
   [re-pressed.core :as rp]
   [knowhere-cafe.events :as events]))


(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  ;; TODO fix it so this isn't needed
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (defroute home "/" []
    (re-frame/dispatch [::events/set-active-panel :home-panel])
    (re-frame/dispatch [::events/set-re-pressed-example nil]))

  (defroute about "/about" []
    (re-frame/dispatch [::events/set-active-panel :about-panel]))

  (defroute radio "/radio" []
    (re-frame/dispatch [::events/set-active-panel :radio-panel]))

  (defroute login "/login" []
    (re-frame/dispatch [::events/set-active-panel :login-panel]))

  (defroute dash "/dash" []
    ;; TODO route guards
    (re-frame/dispatch [::events/set-active-panel :dash-panel]))

  ;; --------------------
  (hook-browser-navigation!))
