#!/usr/bin/env bb

(def input
  (if-let [in (first *command-line-args*)]
    in
    (do (println "Missing folder path")
        (System/exit 1))))

(def in-folder (io/file input))
(def in-folder-path (.toPath in-folder))
(when-not (.exists in-folder)
  (println "Folder does not exist")
  (System/exit 1))
(when-not (.isDirectory in-folder)
  (println "Not a folder")
  (System/exit 1))

(def out-folder
  (io/file
   (-> in-folder-path
      (.toAbsolutePath)
      (.resolveSibling
       (str (.getName in-folder) "-converted"))
      (.toString))))

(when (.exists out-folder)
  (println "The ouput `-converted` folder already exists")
  (System/exit 1))

(.mkdir out-folder)

(def files
  (filter
   #(re-matches #"^\w.*\.(mp3|wav|flac|m4a|ogg)$" (.getName %))
   (seq (.listFiles in-folder))))

(doseq [f files]
  (let [file-path (->
                   (.toPath out-folder)
                   (.resolve (str/replace (.getName f) #"\.[^.]+$" ".ogg"))
                   (str))
        cmd ["ffmpeg"
             "-i"
             (.getAbsolutePath f)
             file-path]]
    (spit (-> (.toPath out-folder)
              (.resolve "playlist.m3u")
              (str))
          (str file-path \newline)
          :append true)
    (apply println cmd)
    (apply shell/sh cmd)))
