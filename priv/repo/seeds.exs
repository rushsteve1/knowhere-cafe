# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     KnowhereCafe.Repo.insert!(%KnowhereCafe.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# Creates the test user, useful during development
Pow.Ecto.Context.create(
  %{username: "test", password: "testpassword", password_confirmation: "testpassword"},
  otp_app: :knowhere_cafe
)
