defmodule KnowhereCafe.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :string
      add :color, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:tags, [:user_id])

    create table(:posts_tags, primary_key: false) do
      add :post_id, references(:posts)
      add :tag_id, references(:tags)
    end

    create unique_index(:posts_tags, [:post_id, :tag_id])

    create table(:users_tags, primary_key: false) do
      add :user_id, references(:users)
      add :tag_id, references(:tags)
    end

    create unique_index(:users_tags, [:user_id, :tag_id])

    create table(:statuses_tags, primary_key: false) do
      add :status_id, references(:statuses)
      add :tag_id, references(:tags)
    end

    create unique_index(:statuses_tags, [:status_id, :tag_id])
  end
end
