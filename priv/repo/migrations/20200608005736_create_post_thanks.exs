defmodule KnowhereCafe.Repo.Migrations.CreatePostThanks do
  use Ecto.Migration

  def change do
    create table(:post_thanks) do
      add :post_id, references(:posts, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:post_thanks, [:post_id])
    create index(:post_thanks, [:user_id])
    create unique_index(:post_thanks, [:user_id, :post_id])
  end
end
