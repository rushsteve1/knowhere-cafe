defmodule KnowhereCafe.Repo.Migrations.AlterUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :aliases, {:array, :string}
      add :avatar, :string
      add :background, :string
      add :bio, :string
    end
  end
end
