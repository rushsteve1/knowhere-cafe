defmodule KnowhereCafe.Repo.Migrations.CreateStatusThanks do
  use Ecto.Migration

  def change do
    create table(:status_thanks) do
      add :status_id, references(:statuses, on_delete: :nothing), null: false
      add :user_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:status_thanks, [:status_id])
    create index(:status_thanks, [:user_id])
    create unique_index(:status_thanks, [:user_id, :status_id])
  end
end
