defmodule KnowhereCafe.Repo.Migrations.CreateCommentThanks do
  use Ecto.Migration

  def change do
    create table(:comment_thanks) do
      add :user_id, references(:users, on_delete: :nothing)
      add :comment_id, references(:comments, on_delete: :nothing)

      timestamps()
    end

    create index(:comment_thanks, [:user_id])
    create index(:comment_thanks, [:comment_id])
    create unique_index(:comment_thanks, [:user_id, :comment_id])
  end
end
