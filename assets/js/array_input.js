// Taken and adapted from
// https://stackoverflow.com/questions/41404526/phoenix-form-field-for-array-model-data-type

window.addEventListener("load", () => {
  setup_remove(document);

  Array.from(document.querySelectorAll(".add-form-field"))
       .forEach(el => {
         el.addEventListener("click", ({target}) => {
           console.info("add");
           const container = document.getElementById(target.dataset.container);
           const index = container.dataset.index;
           const newRow = target.dataset.prototype;

           container.insertAdjacentHTML('beforeend', newRow.replace(/__name__/g, index));
           container.dataset.index = parseInt(container.dataset.index) + 1;

           setup_remove(container);
         });
       });
});

function setup_remove(container) {
  const removeElement = ({target}) => {
    const el = document.getElementById(target.dataset.id);
    if (el) {
      const li = el.parentNode;
      li.parentNode.removeChild(li);
    }
  }

  Array.from(container.querySelectorAll('.remove-form-field')).forEach(el => {
    el.addEventListener("click", (e) => {
      removeElement(e);
    });
  })
}
