// Rain effect taken and adapted from https://codepen.io/arickle/pen/XKjMZY

var rainEnabled = false;

// Add the toggle to the global scope
window.toggleRain = () => {
  rainEnabled = !rainEnabled;
  if (rainEnabled) {
    makeItRain();
  } else {
    clearRain();
  }
  // Add the rain cookie
  document.cookie = "rain=" + rainEnabled;
};

// Clear out everything
function clearRain() {
  const rainEl = document.querySelector('.rain')
  while(rainEl.firstChild) {
    rainEl.removeChild(rainEl.firstChild);
  }
}

function makeItRain() {
  clearRain();

  let increment = 0;
  let drops = "";

  while (increment < 100 && rainEnabled) {
    // Couple random numbers to use for various randomizations
    // Random number between 98 and 1
    var randoHundo = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
    // Random number between 5 and 2
    var randoFiver = (Math.floor(Math.random() * (5 - 2 + 1) + 2));
    // Increment
    increment += randoFiver;
    // Add in a new raindrop with various randomizations to certain CSS properties
    drops += `
      <div class="drop"
        style="
          left: ${increment}%;
          bottom: ${(randoFiver + randoFiver - 1 + 100)}%;
          animation-delay: 0.${randoHundo}s;
          animation-duration: 0.5${randoHundo}s;"
      >
        <div class="stem"
          style="
            animation-delay: 0.${randoHundo}s;
            animation-duration: 0.5${randoHundo}s;"
        ></div>
        <div class="splat"
          style="
            animation-delay: 0.${randoHundo}s;
            animation-duration: 0.5${randoHundo}s;"
        ></div>
      </div>`;
  }

  document.querySelector('.rain').innerHTML = drops;
}

// Runs on load
// Gets the rain cookie and sets rainEnabled
window.addEventListener(
  'load',
  () => {
    const cookieParts = document.cookie.split('; ');
    if (cookieParts[0]) {
      const cookieVal =
            cookieParts
            .find(row => row.startsWith('rain'))
            .split('=')[1];
      rainEnabled = cookieVal === 'true';
      if (rainEnabled) {
        makeItRain();
      }
    }
  }
);
