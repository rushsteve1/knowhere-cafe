defmodule KnowhereCafeWeb.PageControllerTest do
  use KnowhereCafeWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Knowhere Cafe"
  end
end
