defmodule KnowhereCafeWeb.StatusControllerTest do
  use KnowhereCafeWeb.ConnCase

  alias KnowhereCafe.Statuses

  @create_attrs %{content: "some content", user_id: 0}
  @invalid_attrs %{content: nil}

  def fixture(:status) do
    {:ok, status} = Statuses.create_status(@create_attrs)
    status
  end

  describe "index" do
    test "lists all statuses", %{conn: conn} do
      conn = get(conn, Routes.status_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Statuses"
    end
  end

  describe "create status" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.status_path(conn, :create), status: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.status_path(conn, :show, id)

      conn = get(conn, Routes.status_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Status"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.status_path(conn, :create), status: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Status"
    end
  end

  defp create_status(_) do
    status = fixture(:status)
    %{status: status}
  end
end
