# Deploying Knowhere Cafe

Since it is intended that Knowhere Cafe will only every have to be deployed just
once, I have decided to forgo using Docker Compose or Anisble or some other
solution, and instead I'm documenting the steps in this file.

## Server Setup

Knowhere Cafe is running on a Digital Ocean droplet. We're starting at the
lowest tier with the ability to scale up (more powerful sever) and out (multiple
servers and managed database) at a later time.

Since I am a big fan of containers the droplet will be running RancherOS since
that seems to be the only container OS Digital Ocean offers at this time 
(RIP CoreOS).

## Portainer

First up is installing then starting [Portainer](portainer.io) using the
[instructions on their website](https://www.portainer.io/installation/).

This needs to be run only once
```sh
docker volume create portainer_data
```

But this needs to be run every time we want to start Portainer again.
```sh
docker run -d \
  -p 8000:8000 -p 9000:9000 \
  --name=portainer \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v portainer_data:/data \
  portainer/portainer
```

Portainer can be skipped if wanted. But I like having the nice GUI and it makes
fiddling with things easier later.

## Caddy

Next up is the Caddy reverse proxy. I really like Caddy due to its fantastic
defaults like OOTB HTTPS and HTTP/2, and for its very simple configuration with
`Caddyfile`.

Starting Caddy can be done from the Portainer UI or with the following

Like Portainer the volume only has to be created once.
```sh
docker volume create caddy_data
```

Then this for every time you want to start.
```sh
docker run -d \
  -p 80:80 -p 443:443 \
  --name=caddy \
  --restart=always \
  -v caddy_data:/data \
  -v $PWD/Caddyfile:/etc/caddy/Caddyfile \
  caddy
```

The last `-v` line is important, it puts the `Caddyfile` into the container
which configures Caddy appropriately.

### Caddyfile

Since the `Caddyfile` format is so simple and straightforward, I'm putting it
right here in the deploying guide!

```
# Knowhere Cafe itself
knowhere.cafe {
  encode zstd gzip
  reverse_proxy knowhere_cafe:80
}

# The WWW -> @ redirect
www.knowhere.cafe {
  redir knowhere.cafe{uri} permanent
}

# Portainer
containers.knowhere.cafe {
  encode zstd gzip
  reverse_proxy portainer:9000
}
```

## Postgres

Database time! This is much of before, make volume then start container.

```sh
docker volume create postgres_data
```

```sh
docker run -d \
  --name=postgres \
  -e POSTGRES_USER=username \
  -e POSTGRES_PASSWORD=mysecretpassword \
  postgres
```

The only tricky bit here is the `POSTGRES_USER` and `POSTGRES_PASSWORD`
environment variables. These need to be set to good secure values to protect the
database from tampering.

Additionally make sure the database is **NOT** accessible from the outside.

## Knowhere Cafe itself

The tricky part of deploying Knowhere Cafe itself is that the Docker container
is not publicly available. It's quite easy to build yourself, but there's no
public pre-built version on say DockerHub.

So how do we get it to the server? Through the magic of `rsync`!

First build the container image in the usual way
```sh
docker build -t knowhere_cafe .
```

Once it has been built, save the container image to a TAR file.
```sh
docker save -o knowhere_cafe.tar knowhere_cafe
```

Then use `rsync` to copy the file to the server.
```sh
rsync -zhP knowhere_cafe.tar rancher@knowhere.cafe:
```

Once the transfer is done then import the image
```sh
docker load -i knowhere_cafe.tar
```

Now the image has been loaded on the server and it can be started.
```sh
docker run -d \
  --name=knowhere_cafe
  -p 80:80
  -e DATABASE_URL=ecto://USER:PASS@HOST/database
  -e SECRET_KEY_BASE=REALLY_LONG_SECRET
  knowhere_cafe
```

Make sure to set the `DATABASE_URL` and `SECRET_KEY_BASE` environment variables
to the correct values as [described in the Phoenix
docs](https://hexdocs.pm/phoenix/deployment.html). Alternatively these could be
specified when the container was built, but that is less secure.

And yes, this whole process could be streamlined, the save/send/load could be
one SSH command. But this method is far more robust, and much more explicit to
explain and do.
