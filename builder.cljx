#!/usr/bin/env bb

;; .-))--.  Knowhere Cafe
;; i--=--i) Come join us for a drink at the cross-roads of nothing and nowhere...
;; `-___-`  Copyright (c) 2020 Steven vanZyl

;; Knowhere Cafe build script
;; Builds Knowhere Cafe including all code and configs
;; and outputs them to a build/ directory

;; This script is written in Clojure and intended to be run using Babashka
;; https://github.com/borkdude/babashka/
;; It can also be run using
;; clojure builder.cljx

;; These are technically unneeded when using Babashka, but are for compatiblity
;; when running with Clojure (such as on Netlify)
(require '[clojure.java.io :as io]
         '[clojure.java.shell :as shell])

;; Important variables and paths as absolutes
(def website-path
  (.getAbsolutePath (io/file (io/as-relative-path "website/"))))
(def config-path
  (.getAbsolutePath (io/file (io/as-relative-path "config/"))))
(def build-dir (io/file (io/as-relative-path "build/")))
(def build-path (.getAbsolutePath build-dir))


;; 1. Create build dir
;; If the build-dir already exists delete it
(when (.exists build-dir)
  (println "Removing old build/ directory...")
  (shell/sh "rm" "-rf" build-path))

;; Create the build directory
(.mkdir build-dir)

;; 2. Run any tests
;; TODO write tests

;; 3. Build the website and copy it in
(println "Preparing to build Website...")
(shell/with-sh-dir website-path
  ;; If node_modules doesn't exist then assume we need to install dependencies
  (when-not (.exists (io/file (str website-path "/node_modules/")))
    (println "Installing Dependencies...")
    (let [c (shell/sh "npm" "install")]
      (when-not (= 0 (:exit c))
        (println c)
        (System/exit 1)))
    (let [c (shell/sh "lein" "deps")]
      (when-not (= 0 (:exit c))
        (println c)
        (System/exit 1))))
  (println "Building Website...")
  ;; Clean out any old built files directory
  (let [c (shell/sh "lein" "clean")]
    (when-not (= 0 (:exit c))
      (println c)
      (System/exit 1)))
  ;; Build in production mode
  (let [c (shell/sh "lein" "prod")]
    (when-not (= 0 (:exit c))
      (println c)
      (System/exit 1))))

;; Copy the compiled website to the build directory
;; Just using cp here for recursive copying and symlink resolving
(println "Copying built website...")
(shell/sh
 "cp"
 "-LR" ; -L resolves symlinks, -R is recursive copy
 (str website-path "/public/")
 (str build-path))

;; 4. Build the config files
;; TODO convert config files to Dhall
;; for now this just copies the files
(println "Copying config files...")
(io/copy
 (io/file "radio/icecast.xml")
 (io/file (str build-path "/icecast.xml")))
