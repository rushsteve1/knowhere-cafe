defmodule KnowhereCafeWeb.TextUtils do
  @moduledoc """
  A collection of utilities related to text manipulation
  """

  import Phoenix.HTML
  import Phoenix.HTML.Tag

  @doc """
   Parses the text as GFM Markdown using Earmark.
   If it could not be parsed it is passed as is.
  """
  def render_markdown(text, opts \\ [card: false]) do
    # bit hacky
    class =
      "markdown " <>
        if opts[:card] do
          "card"
        else
          ""
        end

    case Earmark.as_html(text, breaks: true, smartypants: false) do
      {:ok, html, _} -> content_tag(:div, raw(html), class: class)
      {:error, _, _} -> text
    end
  end

  @doc """
  Truncates a string to a max length (default 30) adding
  elipsis (...) to the end.
  """
  def truncate(text, opts \\ []) do
    max_length = opts[:max_length] || 30
    omission = opts[:omission] || "..."

    cond do
      not String.valid?(text) ->
        text

      String.length(text) < max_length ->
        text

      true ->
        length_with_omission = max_length - String.length(omission)
        "#{String.slice(text, 0, length_with_omission)}#{omission}"
    end
  end
end
