defmodule KnowhereCafeWeb.TagController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Tags
  alias KnowhereCafe.Tags.Tag

  def index(conn, _params) do
    tags = Tags.list_tags()
    changeset = Tags.change_tag(%Tag{})
    render(conn, "index.html", tags: tags, changeset: changeset)
  end

  def create(conn, %{"tag" => tag_params}) do
    case Tags.create_tag(tag_params) do
      {:ok, tag} ->
        conn
        |> put_flash(:info, "Tag created successfully.")
        |> redirect(to: Routes.tag_path(conn, :show, tag))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    tag = Tags.get_tag!(id)
    render(conn, "show.html", tag: tag)
  end
end
