defmodule KnowhereCafeWeb.UserController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Users

  def index(conn, _params) do
    render(conn, "index.html", root_user: Users.get_root_user!())
  end

  def show(conn, %{"id" => id}) do
    case id do
      "me" ->
        redirect(conn, to: Routes.user_path(conn, :show, conn.assigns.current_user.id))

      _ ->
        render(conn, "show.html",
          user: Users.get_user!(id),
          current: id == "#{conn.assigns.current_user.id}"
        )
    end
  end
end
