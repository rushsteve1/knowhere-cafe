defmodule KnowhereCafeWeb.ThanksController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Thanks

  def create_status_thanks(conn, thanks_params) do
    thanks_params = thanks_params |> Map.put("user_id", conn.assigns.current_user.id)

    case Thanks.create_thanks(thanks_params) do
      {:ok, thanks} ->
        conn
        |> put_flash(:info, "You thanked this status.")
        |> redirect(to: Routes.status_path(conn, :show, thanks.status_id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Could not thank status.")
        |> redirect(to: Routes.status_path(conn, :show, thanks_params["status_id"]))
    end
  end

  def create_post_thanks(conn, thanks_params) do
    thanks_params = thanks_params |> Map.put("user_id", conn.assigns.current_user.id)

    case Thanks.create_thanks(thanks_params) do
      {:ok, thanks} ->
        conn
        |> put_flash(:info, "You thanked this post.")
        |> redirect(to: Routes.post_path(conn, :show, thanks.post_id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Could not thank post.")
        |> redirect(to: Routes.post_path(conn, :show, thanks_params["post_id"]))
    end
  end

  def create_comment_thanks(conn, thanks_params) do
    thanks_params = thanks_params |> Map.put("user_id", conn.assigns.current_user.id)

    # Load the comment to get the Post ID to redirect back to
    post_id = KnowhereCafe.Comments.get_comment!(thanks_params["comment_id"]).post.id

    case Thanks.create_thanks(thanks_params) do
      {:ok, _thanks} ->
        conn
        |> put_flash(:info, "You thanked a comment.")
        |> redirect(to: Routes.post_path(conn, :show, post_id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Could not thank comment.")
        |> redirect(to: Routes.post_path(conn, :show, post_id))
    end
  end
end
