defmodule KnowhereCafeWeb.PageController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Repo
  alias KnowhereCafe.Posts
  alias KnowhereCafe.Statuses
  alias KnowhereCafe.Tags

  def index(conn, _params) do
    posts = Posts.list_posts(10)
    statuses = Statuses.list_statuses(10)
    changeset = Statuses.change_status(%Statuses.Status{})
    changeset = update_in(changeset.data, &Repo.preload(&1, :tags))
    all_tags = Tags.list_tags()

    render(conn, "index.html",
      posts: posts,
      statuses: statuses,
      changeset: changeset,
      all_tags: all_tags
    )
  end

  def about(conn, _params) do
    render(conn, "about.html")
  end
end
