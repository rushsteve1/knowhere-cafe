defmodule KnowhereCafeWeb.PostController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Repo
  alias KnowhereCafe.Posts
  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Tags

  def index(conn, _params) do
    posts = Posts.list_posts()
    render(conn, "index.html", posts: posts)
  end

  def new(conn, params) do
    changeset = Posts.change_post(%Post{})
    changeset = update_in(changeset.data, &Repo.preload(&1, :tags))
    parent = params["parent"]
    all_tags = Tags.list_tags()

    render(conn, "new.html",
      changeset: changeset,
      parent: parent,
      all_tags: all_tags
    )
  end

  def create(conn, %{"post" => post_params} = params) do
    post_params =
      post_params
      |> Map.put("user_id", conn.assigns.current_user.id)
      # Defaults to nil
      |> Map.put("post_id", params["parent"])

    case Posts.create_post(post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html",
          changeset: update_in(changeset.data, &Repo.preload(&1, :tags)),
          parent: post_params["post_params"],
          all_tags: Tags.list_tags()
        )
    end
  end

  def show(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    timeline = Posts.get_post_timeline(id)
    render(conn, "show.html", post: post, timeline: timeline)
  end
end
