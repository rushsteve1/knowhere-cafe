defmodule KnowhereCafeWeb.StatusController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Repo
  alias KnowhereCafe.Statuses
  alias KnowhereCafe.Tags

  def index(conn, _params) do
    statuses = Statuses.list_statuses()
    changeset = Statuses.change_status(%Statuses.Status{})
    changeset = update_in(changeset.data, &Repo.preload(&1, :tags))
    all_tags = Tags.list_tags()

    render(conn, "index.html",
      statuses: statuses,
      changeset: changeset,
      all_tags: all_tags
    )
  end

  def create(conn, %{"status" => status_params}) do
    status_params =
      status_params
      |> Map.put("user_id", conn.assigns.current_user.id)

    case Statuses.create_status(status_params) do
      {:ok, status} ->
        conn
        |> put_flash(:info, "Status created successfully.")
        |> redirect(to: Routes.status_path(conn, :show, status))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    status = Statuses.get_status!(id)
    render(conn, "show.html", status: status)
  end
end
