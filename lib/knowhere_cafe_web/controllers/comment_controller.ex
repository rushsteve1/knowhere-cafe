defmodule KnowhereCafeWeb.CommentController do
  use KnowhereCafeWeb, :controller

  alias KnowhereCafe.Comments

  def create(conn, comment_params) do
    comment_params = comment_params |> Map.put("user_id", conn.assigns.current_user.id)

    case Comments.create_comment(comment_params) do
      {:ok, comment} ->
        conn
        |> put_flash(:info, "Comment added to post.")
        |> redirect(to: Routes.post_path(conn, :show, comment.post_id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Could not comment on post.")
        |> redirect(to: Routes.post_path(conn, :show, comment_params["post_id"]))
    end
  end
end
