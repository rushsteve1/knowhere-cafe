defmodule KnowhereCafeWeb.Router do
  use KnowhereCafeWeb, :router
  use Pow.Phoenix.Router

  use Pow.Extension.Phoenix.Router,
    extensions: [PowInvitation]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_layout, {KnowhereCafeWeb.LayoutView, :not_auth}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler

    plug :put_layout, {KnowhereCafeWeb.LayoutView, :app}
  end

  scope "/" do
    pipe_through :browser

    # Session only (no registration) and extension routes for Pow
    pow_session_routes()
    pow_extension_routes()
  end

  scope "/", KnowhereCafeWeb do
    pipe_through :browser

    get "/about", PageController, :about
  end

  scope "/", Pow.Phoenix, as: "pow" do
    # Registration was disabled so pow_routes() was replaced with
    # pow_session_routes() which means that this is needed to handle the routes.
    pipe_through [:browser, :protected]

    resources "/registration", RegistrationController,
      singleton: true,
      only: [:edit, :update]
  end

  scope "/", KnowhereCafeWeb do
    pipe_through [:browser, :protected]

    get "/", PageController, :index

    resources "/statuses", StatusController, except: [:new, :update, :edit, :delete]
    resources "/posts", PostController, except: [:update, :edit, :delete]
    resources "/tags", TagController, except: [:new, :update, :edit, :delete]

    post "/statuses/:status_id/thanks/", ThanksController, :create_status_thanks
    post "/posts/:post_id/thanks", ThanksController, :create_post_thanks
    post "/comments/:comment_id/thanks", ThanksController, :create_comment_thanks

    post "/posts/:post_id/comment", CommentController, :create

    get "/users", UserController, :index
    get "/users/:id", UserController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", KnowhereCafeWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: KnowhereCafeWeb.Telemetry
    end
  end
end
