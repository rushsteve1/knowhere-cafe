defmodule KnowhereCafeWeb.PostView do
  use KnowhereCafeWeb, :view

  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Comments.Comment
  alias KnowhereCafe.Thanks

  # Renders an item in the timeline view based on what the struct type is.
  defp render_timeline_item(conn, %Post{} = post, index) do
    render("timeline_post.html", conn: conn, post: post, index: index)
  end

  defp render_timeline_item(conn, %Comment{} = comment, index) do
    render("timeline_comment.html", conn: conn, comment: comment, index: index)
  end

  defp render_timeline_item(conn, %Thanks.Post{} = thanks, index) do
    render("timeline_thanks.html", conn: conn, thanks: thanks, index: index)
  end

  defp render_timeline_item(_, _, _), do: nil
end
