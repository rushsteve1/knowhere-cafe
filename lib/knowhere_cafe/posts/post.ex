defmodule KnowhereCafe.Posts.Post do
  use Ecto.Schema
  import Ecto.Changeset
  # Alias itself since the structure is self-referencing
  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Thanks
  alias KnowhereCafe.Comments.Comment
  alias KnowhereCafe.Tags

  schema "posts" do
    field :content, :string
    field :title, :string
    # field :user_id, :id
    # field :post_id, :id

    belongs_to :parent, Post, foreign_key: :post_id
    has_many :children, Post
    belongs_to :user, User
    has_many :thanks, Thanks.Post
    has_many :comments, Comment
    many_to_many :tags, Tags.Tag, join_through: "posts_tags", unique: true

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :content, :user_id, :post_id])
    |> validate_required([:title, :content, :user_id])
    |> put_assoc(:tags, Tags.get_many_tags(attrs["tags_ids"]))
    |> validate_length(:title, min: 2)
    |> validate_length(:title, max: 50)
    |> validate_length(:content, min: 2)
    |> validate_length(:content, max: 2000)
  end
end
