defmodule KnowhereCafe.Tags.Tag do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Statuses.Status

  schema "tags" do
    field :color, :string
    field :name, :string
    # field :user_id, :id
    belongs_to :user, User

    many_to_many :users, User, join_through: "users_tags", unique: true
    many_to_many :posts, Post, join_through: "posts_tags", unique: true
    many_to_many :statuses, Status, join_through: "statuses_tags", unique: true

    timestamps()
  end

  @doc false
  def changeset(tag, attrs) do
    tag
    |> cast(attrs, [:name, :color])
    |> validate_required([:name, :color])
    |> validate_length(:name, min: 2)
    |> validate_length(:name, max: 25)
  end
end
