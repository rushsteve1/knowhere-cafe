defmodule KnowhereCafe.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  use Pow.Ecto.Schema,
    user_id_field: :username

  use Pow.Extension.Ecto.Schema,
    extensions: [PowInvitation]

  alias KnowhereCafe.Statuses.Status
  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Tags

  schema "users" do
    pow_user_fields()

    field :aliases, {:array, :string}
    field :avatar, :string
    field :background, :string
    field :bio, :string

    has_many :statuses, Status
    has_many :posts, Post
    many_to_many :tags, Tags.Tag, join_through: "users_tags", unique: true, on_replace: :delete

    timestamps()
  end

  def changeset(%Ecto.Changeset{} = changeset, attrs) do
    changeset
    |> pow_changeset(attrs)
    |> pow_extension_changeset(attrs)
    |> validate_length(:username, max: 20)
  end

  def changeset(%KnowhereCafe.Users.User{} = user, attrs) do
    user
    |> KnowhereCafe.Repo.preload(:tags)
    |> pow_changeset(attrs)
    |> pow_extension_changeset(attrs)
    |> cast(attrs, [:avatar, :background, :aliases, :bio])
    |> put_assoc(:tags, Tags.get_many_tags(attrs["tags_ids"]))
    |> validate_length(:username, max: 20)
  end
end
