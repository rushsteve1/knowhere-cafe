defmodule KnowhereCafe.Users do
  @moduledoc """
  The users context.

  This module is deeply related to the Pow authentication system that Knowhere
  is built with and acts as the Context for that as well.
  """

  alias KnowhereCafe.Repo
  alias KnowhereCafe.Users.User
  use Pow.Ecto.Context,
    repo: KnowhereCafe.Repo,
    user: KnowhereCafe.Users.User

  @doc """
  Returns a list of all users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user(123)
      %User{}

      iex> get_user(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id) do
    User
    |> Repo.get!(id)
    |> Repo.preload([
      :invited_by,
      :invited_users,
      :tags,
      posts: [:user],
      statuses: [:user]
    ])
  end

  @doc """
  Gets the root user with ID 1 that was not invited by another user.
  Used to traverse the invitation tree.

  Raises `Ecto.NoResultsError` if the root User does not exist.

  ## Examples

      iex> get_root_user()
      %User{}

  """
  def get_root_user! do
    User
    |> Repo.get!(1)
    |> Repo.preload(:invited_users)
  end

  @doc """
  Gets the users invited by the given user.
  This is largely an wrapper to `Repo.preload(user, :invited_users)`
  but keeps the business logic in this module.

  ## Examples

      iex> get_invited_users(%User{})
      [%User{}, ...]

  """
  def get_invited_users(%User{} = user) do
    Repo.preload(user, :invited_users).invited_users
  end

  # ===========================
  # Pow Callbacks and functions
  # ===========================

  # Overrides the way Pow gets the user to allow for preloading
  @impl true
  def get_by(clauses) do
    clauses
    |> pow_get_by()
    |> preload_profile()
  end

  @impl true
  def create(params), do: pow_create(params)

  @impl true
  def authenticate(params) do
    params
    |> pow_authenticate()
    |> preload_profile()
  end

  @impl true
  def update(user, params), do: pow_update(user, params)

  @impl true
  def delete(user), do: pow_delete(user)

  defp preload_profile(nil), do: nil
  defp preload_profile(user) do
    Repo.preload(user, :tags)
  end
end
