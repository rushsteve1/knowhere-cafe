defmodule KnowhereCafe.Thanks.Status do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Statuses.Status

  schema "status_thanks" do
    # field :status_id, :id
    # field :user_id, :id
    belongs_to :user, User
    belongs_to :status, Status

    timestamps()
  end

  @doc false
  def changeset(status, attrs) do
    status
    |> cast(attrs, [:user_id, :status_id])
    |> validate_required([:user_id, :status_id])
    |> unique_constraint([:user_id, :status_id])
  end
end
