defmodule KnowhereCafe.Thanks.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Comments.Comment

  schema "comment_thanks" do
    # field :user_id, :id
    # field :comment_id, :id
    belongs_to :user, User
    belongs_to :comment, Comment

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:user_id, :comment_id])
    |> validate_required([:user_id, :comment_id])
    |> unique_constraint([:user_id, :comment_id])
  end
end
