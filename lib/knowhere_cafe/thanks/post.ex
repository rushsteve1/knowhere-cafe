defmodule KnowhereCafe.Thanks.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Posts.Post

  schema "post_thanks" do
    # field :post_id, :id
    # field :user_id, :id
    belongs_to :user, User
    belongs_to :post, Post

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:user_id, :post_id])
    |> validate_required([:user_id, :post_id])
    |> unique_constraint([:user_id, :post_id])
  end
end
