defmodule KnowhereCafe.Thanks do
  @moduledoc """
  The Thanks context.

  "Thanks" are akin to "likes" on other platforms, but intentionally given a
  different wording. Thanks are largely a many-to-many relation, but since they
  require timestamps and other minor features they are custom implmented instead
  of using Ectos :many_to_many. This also gives room to expand into some more
  interesteing ideas of mine at a later time.
  """

  import Ecto.Query, warn: false
  alias KnowhereCafe.Repo

  # NOTE these are not the same as an actual Post or Status struct
  alias KnowhereCafe.Thanks.Post
  alias KnowhereCafe.Thanks.Status
  alias KnowhereCafe.Thanks.Comment

  @doc """
  Returns the list of thanks belonging to a status.

  ## Examples

      iex> list_thanks_on_status(id)
      [%Thanks.Status{}, ...]

  """
  def list_thanks_on_status(id) do
    Repo.all(from t in Status, where: t.status_id == ^id)
    |> Repo.preload(:user)
  end

  @doc """
  Returns the list of thanks belonging to a post.

  ## Examples

      iex> list_thanks_on_post(id)
      [%Thanks.Post{}, ...]
  """
  def list_thanks_on_post(id) do
    Repo.all(from t in Post, where: t.post_id == ^id)
    |> Repo.preload(:user)
  end

  @doc """
  Returns the list of thanks belonging to a comment.

  ## Examples

      iex> list_thanks_on_comment(id)
      [%Thanks.Comment{}, ...]
  """
  def list_thanks_on_comment(id) do
    Repo.all(from t in Comment, where: t.comment_id == ^id)
    |> Repo.preload(:user)
  end

  @doc """
  Creates a thanks.

  This function pattern matches on the map passed into it and performs the
  corresponding operation for every type of thanks.

  ## Examples

      iex> create_thanks(attrs)
      {:ok, %Thanks.Post{}}

  """
  def create_thanks(%{"status_id" => _} = attrs) do
    %Status{}
    |> Status.changeset(attrs)
    |> Repo.insert()
  end

  def create_thanks(%{"post_id" => _} = attrs) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  def create_thanks(%{"comment_id" => _} = attrs) do
    %Comment{}
    |> Comment.changeset(attrs)
    |> Repo.insert()
  end

  def create_thanks(_) do
    # TODO better error handling for this
    raise ArgumentError, message: "Invalid struct to create_thanks"
  end
end
