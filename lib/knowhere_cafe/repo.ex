defmodule KnowhereCafe.Repo do
  use Ecto.Repo,
    otp_app: :knowhere_cafe,
    adapter: Ecto.Adapters.Postgres
end
