defmodule KnowhereCafe.Posts do
  @moduledoc """
  The Posts context.
  """

  import Ecto.Query, warn: false
  alias KnowhereCafe.Repo

  alias KnowhereCafe.Posts.Post

  @doc """
  Returns the list of posts. Can be limited.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts do
    Repo.all(from p in Post, order_by: [desc: p.inserted_at])
    |> Repo.preload(:user)
  end

  def list_posts(limit) do
    Repo.all(from p in Post, limit: ^limit, order_by: [desc: p.inserted_at])
    |> Repo.preload(:user)
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id) do
    Post
    |> Repo.get!(id)
    |> Repo.preload([
      :user,
      :children,
      :tags,
      parent: [:user],
      comments: [:user],
      thanks: [:user]
    ])
  end

  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    Repo.delete(post)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{data: %Post{}}

  """
  def change_post(%Post{} = post, attrs \\ %{}) do
    Post.changeset(post, attrs)
  end

  @doc """
  Returns the list of posts belonging to a parent post.

  ## Examples

      iex> list_posts_on_parent()
      [%Post{}, ...]

  """
  def list_posts_on_parent(id) do
    Repo.all(from p in Post, where: p.post_id == ^id)
    |> Repo.preload(:user)
  end

  @doc """
  Returns a series of Posts, Comments, and Thanks associated with this post
  ordered by their insertion time. This forms a "timeline" of the activity
  around a post, which is the primary view of the post.

  ## Examples

      iex> post_timeline(%Post{})
      [%Post{}, %Comment{}, %Thanks{}, ...]

      iex> post_timeline(id)
      [%Post{}, %Comment{}, %Thanks{}, ...]

  """
  def post_timeline(%Post{:children => children, :comments => comments, :thanks => thanks}) do
    Enum.sort(
      children ++ comments ++ thanks,
      &(&1.inserted_at < &2.inserted_at)
    )
  end

  def get_post_timeline(id) do
    # Alternatively some kind of UNION query or something could likely
    # do what I want here. But I had issues with that, so this is easier.
    children = KnowhereCafe.Comments.list_comments_on_post(id)
    comments = list_posts_on_parent(id)
    thanks = KnowhereCafe.Thanks.list_thanks_on_post(id)

    Enum.sort(
      children ++ comments ++ thanks,
      &(NaiveDateTime.compare(&1.inserted_at, &2.inserted_at) == :lt)
    )
  end
end
