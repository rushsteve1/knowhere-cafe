defmodule KnowhereCafe.Comments.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Posts.Post
  alias KnowhereCafe.Thanks

  schema "comments" do
    field :content, :string
    # field :user_id, :id
    # field :post_id, :id
    belongs_to :user, User
    belongs_to :post, Post
    has_many :thanks, Thanks.Comment

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:content, :user_id, :post_id])
    |> validate_required([:content, :user_id, :post_id])
    |> validate_length(:content, min: 2)
    |> validate_length(:content, max: 500)
  end
end
