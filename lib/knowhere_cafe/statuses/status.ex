defmodule KnowhereCafe.Statuses.Status do
  use Ecto.Schema
  import Ecto.Changeset
  alias KnowhereCafe.Users.User
  alias KnowhereCafe.Tags

  schema "statuses" do
    field :content, :string
    # field :user_id, :id
    belongs_to :user, User
    has_many :thanks, KnowhereCafe.Thanks.Status
    many_to_many :tags, Tags.Tag, join_through: "statuses_tags", unique: true

    timestamps()
  end

  @doc false
  def changeset(status, attrs) do
    status
    |> cast(attrs, [:user_id, :content])
    |> validate_required([:user_id, :content])
    |> put_assoc(:tags, Tags.get_many_tags(attrs["tags_ids"]))
    |> validate_length(:content, min: 2)
    |> validate_length(:content, max: 200)
  end
end
