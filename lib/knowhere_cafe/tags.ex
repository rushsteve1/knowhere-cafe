defmodule KnowhereCafe.Tags do
  @moduledoc """
  The Tags context.
  """

  import Ecto.Query, warn: false
  alias KnowhereCafe.Repo

  alias KnowhereCafe.Tags.Tag

  @doc """
  Returns the list of tags.

  ## Examples

      iex> list_tags()
      [%Tag{}, ...]

  """
  def list_tags do
    Repo.all(Tag)
  end

  @doc """
  Gets a single tag.

  Raises `Ecto.NoResultsError` if the Tag does not exist.

  ## Examples

      iex> get_tag!(123)
      %Tag{}

      iex> get_tag!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tag!(id) do
    Tag
    |> Repo.get!(id)
    |> Repo.preload([:users, posts: [:user], statuses: [:user]])
  end

  @doc """
  Creates a tag.

  ## Examples

      iex> create_tag(%{field: value})
      {:ok, %Tag{}}

      iex> create_tag(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tag(attrs \\ %{}) do
    %Tag{}
    |> Tag.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a tag.

  ## Examples

      iex> update_tag(tag, %{field: new_value})
      {:ok, %Tag{}}

      iex> update_tag(tag, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tag(%Tag{} = tag, attrs) do
    tag
    |> Tag.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a tag.

  ## Examples

      iex> delete_tag(tag)
      {:ok, %Tag{}}

      iex> delete_tag(tag)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tag(%Tag{} = tag) do
    Repo.delete(tag)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tag changes.

  ## Examples

      iex> change_tag(tag)
      %Ecto.Changeset{data: %Tag{}}

  """
  def change_tag(%Tag{} = tag, attrs \\ %{}) do
    Tag.changeset(tag, attrs)
  end

  @doc """
  Returns multiple tags based on the ids given. Can take a single value or an
  array of values. Types will be cast appropriately if possible.

  ## Examples

      iex> get_many_tags(1)
      [%Tag{}]

      iex> get_many_tags([1, 2])
      [%Tag{}, %Tag{}]

      iex> get_many_tags()
      []

  """
  def get_many_tags(nil), do: []
  def get_many_tags(""), do: []

  def get_many_tags([_ | _] = ids) do
    Repo.all(from t in Tag, where: t.id in type(^ids, {:array, :id}))
  end

  def get_many_tags(id) do
    Repo.all(from t in Tag, where: t.id == type(^id, :id))
  end

 @doc """
 Returns the number of entries related to this tag.

 ## Examples

     iex> count_related(tag)
     5

 """
 def count_related(tag) do
   tag
   |> Repo.preload([:users, :posts, :statuses])
   |> counter()
 end

 defp counter(%Tag{:users => u, :posts => p, :statuses => s}) do
   length(u) + length(p) + length(s)
 end
end
