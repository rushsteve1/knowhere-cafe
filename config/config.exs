# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :knowhere_cafe,
  ecto_repos: [KnowhereCafe.Repo]

# Configures the endpoint
config :knowhere_cafe, KnowhereCafeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "iXKV8MAo7/dd/do3NgpM8uyGDTkBsJChaCOsfGkk19h+Vda+Y495ji0ep4H9p8Ag",
  render_errors: [view: KnowhereCafeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: KnowhereCafe.PubSub,
  live_view: [signing_salt: "kKiTGxSl"]

# Configures Pow authentication
config :knowhere_cafe, :pow,
  user: KnowhereCafe.Users.User,
  repo: KnowhereCafe.Repo,
  web_module: KnowhereCafeWeb,
  users_context: KnowhereCafe.Users,
  extensions: [PowInvitation, PowPersistentSession],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
