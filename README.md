# [Knowhere Cafe](https://knowhere.cafe)

```
.-))--.  Knowhere Cafe
i--=--i) Come join us for a drink at the cross-roads of nothing and nowhere...
`-___-`
```

*Private community forums and more...*

Knowhere Cafe is a private invite-only community with some novel features
intended to foster a better sense of community and participation.

**Current Development Status:** Alpha

This code is not intended to be re-usable across different installations.

## Contributing

Contributions of any kinda are greatly appreciated!

### Submitting Issues

Issue Tracker: https://todo.sr.ht/~rushsteve1/knowhere_cafe

New issues can be created by emailing `~rushsteve1/knowhere_cafe@todo.sr.ht`.
Only plain text emails are supported.

More information about this process is detailed in the
[sr.ht tutorials here](https://man.sr.ht/todo.sr.ht/).

### Code Contributions

Git Repository: https://git.sr.ht/~rushsteve1/knowhere_cafe

Patches are accepted via email by sending them to
rushsteve1 at rushsteve1 dot us.

More information about this process is detailed in the
[sr.ht tutorials here](https://man.sr.ht/tutorials).

Please note that all contributions must fall under the terms of the Apache 2.0
license this project is under.

## Phoenix

> These instructions were provided by Phoenix when the project was initially
> generated.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets`
    directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please
[check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

### Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
